# jettyrest-gradle-groovy

Java 8 works best for this.


## How to test:

    ./gradlew test

## How to build a fat jar:

    ./gradlew shadowJar

## How to run the fat jar:

    java -jar build/libs/martin-app-all.jar

## Try the server like this:

    curl localhost:4444/bankaccounts
    curl localhost:4444/bankaccounts/default


