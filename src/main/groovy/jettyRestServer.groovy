/*********************
This is a groovy script with an embedded jetty server.
Use java 8.
Martin Wangel for SpeedLedger
**********************/

import javax.servlet.http.*
import org.eclipse.jetty.server.*
import org.eclipse.jetty.server.handler.AbstractHandler 
import groovy.json.* 

/** Extending the Jetty handler class to provide some SpeedLedger functionality as simply as possible. */
class MyHandler extends AbstractHandler {
	def data = [
	    ["id": 1 , "number": "1357756" , "name":"Personal account" , "creditcard": false , "synthetic": false , "balance": 1202.14]   ,
	    ["id": 2 , "number": "2446987" , "name":"Business account" , "creditcard": false , "synthetic": false , "balance": 34057.00]  ,
	    ["id": 3 , "number": "9981644" , "name":"Credit card"      , "creditcard": true  , "synthetic": false , "balance": -10057.00] ,
	    ["id": 4 , "number": ""        , "name":"Expense claims"   , "creditcard": false , "synthetic": true  , "balance": 0] ]

	@Override
	void handle(String target, Request jettyRequest, HttpServletRequest request, HttpServletResponse response) {
		//println "target: $target" // Request string between the host and the parameters.
		//println "jr.method: ${jettyRequest.method}" // GET, PUT, ...
		jettyRequest.handled = true
		response.contentType = "application/json;charset=utf-8"

		switch( target ) {
			case "/bankaccounts":
				response.writer << JsonOutput.prettyPrint( JsonOutput.toJson(data) ) << '\n' 
				break;

			case "/bankaccounts/default":
				def defaultResult = findDefaultAccount( data )
				response.writer << JsonOutput.prettyPrint( JsonOutput.toJson( [ "default": (defaultResult ?: "null")] ) ) << '\n' 
				break;

			default:
				response.status = HttpServletResponse.SC_NOT_FOUND
		}
	}


	/***
	* The rules: 
	* o If there is a bank account with a positive balance that is at least
	*   twice as high as all other bank accounts, return the `id` of that account.
	*   This means "...as ANY other bank account's...", not compared to double the sum of other balances.
	* o If there is only a single bank account, return the id of that account.
	* o Synthetic bank accounts can never be chosen as default account.
	* o Accounts with negative balance can never be chosen as default accounts.
	* o If no account satisfied the above, return `null`.
	* @param list A collection of things that respond to (id:number, balance:number, synthetic:boolean) with non-null values.
	* @return A number, or NULL if no match was found.
	*/
	def findDefaultAccount( list ) {
		// Remove synthetic and negative accounts. Handle 0 or 1 result size cases.
		def filtered = list?.findAll { (!it.synthetic) && (it.balance >= 0) }
		if( !filtered ) return null
		if( filtered.size() == 1 ) return filtered[0].id

		// Handle larger result sets. First the case of the double amount. (2*0 >= 0 btw so 0 is ok)
		def sortedBalances = filtered.collect { it.balance }.sort { -it }
		if( sortedBalances[0] >= 2 * sortedBalances[1] ) {
			return filtered.find { it.balance == sortedBalances[0] }.id
		}

		null
	}
}

// Main entry point to the program.
def server = new Server(4444)
server.handler = new MyHandler()

server.start()
server.join()
