import spock.lang.*

class firstSpec extends Specification {

	@Unroll
	def "Default account has positive balance: result is #expectedResult for input: #data"() {
		given:
			def handler = new MyHandler()

		when:
			def callResult = handler.findDefaultAccount( data )

		then:
			callResult == expectedResult

		where:
			expectedResult | data
			null | null
			null | [[id:1, balance:-1, synthetic:true]]
			null | [[id:1, balance:-1, synthetic:false]]
			null | [[id:1, balance:-1, synthetic:true], [id:2, balance:-1_200_032_759_676, synthetic:true]]
			null | [[id:1, balance:-1, synthetic:true], [id:2, balance:-1_200_032_759_676, synthetic:false]]
			null | [[id:1, balance:-1, synthetic:false], [id:2, balance:-1_200_032_759_676, synthetic:false]]
			null | [[id:1, balance:-1, synthetic:false], [id:2, balance:-1_200_032_759_676, synthetic:false], [id:3, balance:-1E45, synthetic:false]]
			3    | [[id:1, balance:-1, synthetic:false], [id:2, balance:-1_200_032_759_676, synthetic:false], [id:3, balance:0, synthetic:false]]
			3    | [[id:1, balance:-1, synthetic:false], [id:2, balance:-1_200_032_759_676, synthetic:false], [id:3, balance:4, synthetic:false]]
	}


	@Unroll
	def "Default account is not synthetic: result is #expectedResult for input: #data"() {
		given:
			def handler = new MyHandler()

		when:
			def callResult = handler.findDefaultAccount( data )

		then:
			callResult == expectedResult

		where:
			expectedResult | data
			null | null
			null | [[id:1, balance:1, synthetic:true]]
			1    | [[id:1, balance:1, synthetic:false]]
			null | [[id:1, balance:1, synthetic:true], [id:2, balance:-1_200_032_759_676, synthetic:true]]
			2    | [[id:1, balance:1, synthetic:true], [id:2, balance: 1_200_032_759_676, synthetic:false]]
			1    | [[id:1, balance:1, synthetic:false], [id:2, balance:-1_200_032_759_676, synthetic:false]]
	}

	@Unroll
	def "Default account has at least double balance: result is #expectedResult for input: #data"() {
		given:
			def handler = new MyHandler()

		when:
			def callResult = handler.findDefaultAccount( data )

		then:
			callResult == expectedResult

		where:
			expectedResult | data
			null | [[id:1, balance:7, synthetic:false], [id:2, balance:4, synthetic:false]]
			1    | [[id:1, balance:8, synthetic:false], [id:2, balance:4, synthetic:false]]
			1    | [[id:1, balance:9, synthetic:false], [id:2, balance:4, synthetic:false]]
			2    | [[id:1, balance:1, synthetic:false], [id:2, balance:4, synthetic:false]]
			2    | [[id:1, balance:1, synthetic:false], [id:2, balance:2, synthetic:false]]
			null | [[id:1, balance:3, synthetic:false], [id:2, balance:4, synthetic:false]]
	}

	def "Default account is null for empty input"() {
		given:
			def handler = new MyHandler()

		when:
			def callResult = handler.findDefaultAccount( [] )

		then:
			callResult == null		
	}

	def "Default account is null for NULL input"() {
		given:
			def handler = new MyHandler()

		when:
			def callResult = handler.findDefaultAccount( null )

		then:
			callResult == null		
	}
}
